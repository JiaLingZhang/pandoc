Source: pandoc
Section: text
Priority: optional
Maintainer: Debian Haskell Group <debian-haskell@lists.debian.org>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
 Kiwamu Okabe <kiwamu@debian.or.jp>,
 Clint Adams <clint@debian.org>,
Build-Depends:
 alex,
 bash-completion,
 cdbs,
 debhelper,
 dh-buildinfo,
 ghc,
 ghc-prof,
 happy,
 haskell-devscripts,
 libghc-hslua-cli-dev (<< 1.5) [!ppc64 !ppc64el],
 libghc-hslua-cli-dev (>= 1.4.1) [!ppc64 !ppc64el],
 libghc-hslua-cli-prof [!ppc64 !ppc64el],
 libghc-pandoc-dev (>= 3.1.3),
 libghc-pandoc-lua-engine-dev (<< 0.3) [!loong64 !ppc64 !ppc64el],
 libghc-pandoc-lua-engine-dev (>= 0.2) [!loong64 !ppc64 !ppc64el],
 libghc-pandoc-lua-engine-prof [!ppc64 !ppc64el],
 libghc-pandoc-prof,
 libghc-temporary-dev (<< 1.4) [!ppc64 !ppc64el],
 libghc-temporary-prof [!ppc64 !ppc64el],
 pandoc-data,
Standards-Version: 4.6.2
Homepage: https://pandoc.org/
Vcs-Git: https://salsa.debian.org/haskell-team/pandoc.git
Vcs-Browser: https://salsa.debian.org/haskell-team/pandoc
Rules-Requires-Root: no

Package: pandoc
Architecture: any
Depends:
 pandoc-data (<< ${pandoc:Lib}.~),
 pandoc-data (>= ${pandoc:Lib}),
 ${cdbs:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${cdbs:Recommends},
Suggests:
 ${cdbs:Suggests},
Provides:
 pandoc-abi (= ${pandoc:Abi}),
Multi-Arch: foreign
Description: general markup converter
 Pandoc is a Haskell library for converting
 from one markup format to another,
 and a command-line tool that uses this library.
 The formats it can handle include
  * light markup formats
    (many variants of Markdown, reStructuredText, AsciiDoc,
     Org-mode, Muse, Textile, txt2tags)
  * HTML formats (HTML 4 and 5)
  * Ebook formats (EPUB v2 and v3, FB2)
  * Documentation formats (GNU TexInfo, Haddock)
  * Roff formats (man, ms)
  * TeX formats (LaTeX, ConTeXt)
  * Typst
  * XML formats
    (DocBook 4 and 5, JATS, TEI Simple, OpenDocument)
  * Outline formats (OPML)
  * Bibliography formats (BibTeX, BibLaTeX, CSL JSON, CSL YAML, RIS)
  * Word processor formats (Docx, RTF, ODT)
  * Interactive notebook formats (Jupyter notebook ipynb)
  * Page layout formats (InDesign ICML)
  * Wiki markup formats
    (MediaWiki, DokuWiki, TikiWiki, TWiki,
     Vimwiki, XWiki, ZimWiki, Jira wiki, Creole)
  * Slide show formats
    (LaTeX Beamer, PowerPoint, Slidy,
     reveal.js, Slideous, S5, DZSlides)
  * Data formats (CSV and TSV tables)
  * PDF (via external programs such as pdflatex or wkhtmltopdf)
 .
 Pandoc can convert mathematical content in documents
 between TeX, MathML, Word equations, roff eqn, typst,
 and plain text.
 It includes a powerful system
 for automatic citations and bibliographies,
 and it can be customized extensively using templates, filters,
 and custom readers and writers written in Lua.
 .
 This package contains the pandoc tool.
 .
 Some uses of Pandoc require additional packages:
  * SVG content in PDF output requires librsvg2-bin.
  * YAML metadata in TeX-related output requires texlive-latex-extra.
  * *.hs filters not set executable requires ghc.
  * *.js filters not set executable requires nodejs.
  * *.php filters not set executable requires php.
  * *.pl filters not set executable requires perl.
  * *.py filters not set executable requires python.
  * *.rb filters not set executable requires ruby.
  * *.r filters not set executable requires r-base-core.
  * LaTeX output, and PDF output via PDFLaTeX,
    require texlive-latex-recommended.
  * XeLaTeX output, and PDF output via XeLaTeX, require texlive-xetex.
  * LuaTeX output, and PDF output via LuaTeX, require texlive-luatex.
  * ConTeXt output, and PDF output via ConTeXt, require context.
  * PDF output via wkhtmltopdf requires wkhtmltopdf.
  * Roff man and roff ms output, and PDF output via roff ms,
    require groff.
  * MathJax-rendered equations require libjs-mathjax.
  * KaTeX-rendered equations require node-katex.
  * option --csl may use styles in citation-style-language-styles.
